//
//  DBManager.h
//  DBConcurrencyPOC
//
//  Created by Anshad M K on 22/02/18.
//  Copyright © 2018 Anshad M K. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject

+ (DBManager *)sharedManager;

- (BOOL)createDb;

- (void) saveData;

- (void)concurrentRecordsInsertion;

- (void)sequentialRecordInsertion;

- (void)createConnectionTable_A;

- (void)createConnectionTable_B;


@end
