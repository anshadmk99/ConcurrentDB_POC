//
//  DBManager.m
//  DBConcurrencyPOC
//
//  Created by Anshad M K on 22/02/18.
//  Copyright © 2018 Anshad M K. All rights reserved.
//

#import "DBManager.h"
#import <sqlite3.h>


@interface DBManager()

@property (nonatomic, strong) dispatch_group_t dbGroup;
@property (nonatomic) sqlite3 *connection_A;
@property (nonatomic) sqlite3 *connection_B;



@property (nonatomic, strong) dispatch_queue_t dbQueue;

@end

@implementation DBManager

+ (instancetype)sharedManager{
    
    static DBManager *dataBaseManager = nil;;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        dataBaseManager = [[self alloc] init];
        
    });
    return dataBaseManager;
}


- (instancetype)init{
    
    self = [super init];
    if(self){
        [self createDb];
//        [self createSQLiteConnection];
        self.dbQueue = dispatch_queue_create("com.schoolspeak.ios.queue", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (BOOL)createDb{
    
    sqlite3 *concurrentDB = nil;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSString *databasePath = [self databasePath];
    if ([filemgr fileExistsAtPath: databasePath ] == NO) {
        const char *dbpath = [databasePath cStringUsingEncoding:NSUTF8StringEncoding];
        
        if (sqlite3_open_v2(dbpath, &concurrentDB, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE , NULL) == SQLITE_OK) {
            
            char *error = nil;
            
            char const *sql_stmt = "CREATE TABLE IF NOT EXISTS Table_A (ID_A INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, ADDRESS TEXT, PHONE TEXT, AGE TEXT, COMPANY TEXT, DESIG TEXT);";
            
            if (sqlite3_exec(concurrentDB,sql_stmt, NULL,NULL, &error) != SQLITE_OK){
                NSLog(@"%s", error);
            }
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS Table_B (ID_B INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, ADDRESS TEXT, PHONE TEXT, AGE TEXT, COMPANY TEXT, DESIG TEXT);";
            
            if (sqlite3_exec(concurrentDB,sql_stmt, NULL,NULL, &error) != SQLITE_OK){
                NSLog(@"%s", error);
            }
            
        }
        else {
            NSLog(@"Failed to open database");
        }
        sqlite3_close(concurrentDB);
    }
    return YES;
}

//- (void)createSQLiteConnection{
//
//    sqlite3 *concurrentDB = nil;
//    char const *dbpath2 = [[self databasePath] cStringUsingEncoding:NSUTF8StringEncoding];
//    int rc = sqlite3_open_v2(dbpath2, &concurrentDB, SQLITE_OPEN_READWRITE, NULL);
//    if(rc != SQLITE_OK){
//
//        NSLog(@"Connection failed");
//
//    }
//    self.concurrentDB = concurrentDB;
//}




- (NSString *)databasePath{
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"concurrent.db"]];
    return databasePath;
}


- (void)createConnectionTable_A{
    
    sqlite3 *connection_A = nil;
    char const *dbpath2 = [[self databasePath] cStringUsingEncoding:NSUTF8StringEncoding];
    int rc = sqlite3_open_v2(dbpath2, &connection_A, SQLITE_OPEN_READWRITE, NULL);
    if(rc != SQLITE_OK){
        
        NSLog(@"Connection failed");
        
    }
    self.connection_A = connection_A;
    [self clearTable_A];
    
}

- (void)createConnectionTable_B{
    
    sqlite3 *connection_B = nil;
    char const *dbpath2 = [[self databasePath] cStringUsingEncoding:NSUTF8StringEncoding];
    int rc = sqlite3_open_v2(dbpath2, &connection_B, SQLITE_OPEN_READWRITE, NULL);
    if(rc != SQLITE_OK){
        
        NSLog(@"Connection failed");
        
    }
    self.connection_B = connection_B;
    
    [self clearTable_B];
}

- (void)clearTable_A{
    
    char *errMsg = nil;
    char *sql_query = "DELETE FROM Table_A;";
    int rc = sqlite3_exec(self.connection_A, sql_query ,NULL,NULL,&errMsg);
    if(rc == SQLITE_OK)
    {
        NSLog(@"Table_A records deleted");
    }
    
}

- (void)clearTable_B{
    
    char *errMsg = nil;
    char *sql_query = "DELETE FROM Table_B;";
    int rc = sqlite3_exec(self.connection_B, sql_query ,NULL,NULL,&errMsg);
    if(rc == SQLITE_OK)
    {
        NSLog(@"Table_B records deleted");
    }
}



- (void)insertRecordsTable_A{
    
    int rc = 0;
    char *errMsg = nil;
    char const *sql_query = nil;
    
    
    NSLog(@"A insertion started ");
    for(NSInteger i=1; i<=30;i++){
        
        NSString *name = [NSString stringWithFormat:@"EmployeeA_%ld",(long)i];
        sql_query = [[NSString stringWithFormat:@"INSERT INTO Table_A (NAME, ADDRESS, PHONE, AGE, COMPANY, DESIG) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",name,@"Address_A",@"987456123",@"25",@"Microsoft",@"Engineer"] UTF8String];
        
        
        rc = sqlite3_exec(self.connection_A, sql_query ,NULL,NULL,&errMsg);
        if(SQLITE_OK != rc)
        {
            NSLog(@"Failed to insert record  rc:%d, msg=%s",rc,errMsg);
            
            while (sqlite3_exec(self.connection_A, sql_query ,NULL,NULL,&errMsg) != SQLITE_OK);
            NSLog(@"Failed insertion RE-WRITED");
        }
        [NSThread sleepForTimeInterval:0.4];
    }
    NSLog(@"A insertion completed");
}



- (void)insertRecordsTable_B{
    int rc = 0;
    char *errMsg = nil;
    NSLog(@"B insertion started ");
    
    
    char const *sql_query = nil;
    for(NSInteger i=1; i<=30;i++){
        
        NSString *name = [NSString stringWithFormat:@"EmployeeB_%ld",(long)i];
        sql_query = [[NSString stringWithFormat:@"INSERT INTO Table_B (NAME, ADDRESS, PHONE, AGE, COMPANY, DESIG) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",name,@"Address_B",@"123456789",@"22",@"SchoolSpeak",@"Engineer"] UTF8String];
        
        
        rc = sqlite3_exec(self.connection_B, sql_query ,NULL,NULL,&errMsg);
        if(SQLITE_OK != rc)
        {
            NSLog(@"Failed to insert record  rc:%d, msg=%s",rc,errMsg);
            
            while (sqlite3_exec(self.connection_B, sql_query ,NULL,NULL,&errMsg) != SQLITE_OK);
            NSLog(@"Failed insertion RE-WRITED");
        }
        [NSThread sleepForTimeInterval:0.5];
    }
    NSLog(@"B insertion completed");
}



- (void)sequentialRecordInsertion{
    
    [self insertRecordsTable_A];
    [self insertRecordsTable_B];
}

- (void)concurrentRecordsInsertion{
    
    dispatch_async(self.dbQueue, ^{
        
        [self insertRecordsTable_A];
    });
    dispatch_async(self.dbQueue, ^{
        
        [self insertRecordsTable_B];
    });
}

@end
