//
//  ViewController.m
//  DBConcurrencyPOC
//
//  Created by Anshad M K on 22/02/18.
//  Copyright © 2018 Anshad M K. All rights reserved.
//

#import "ViewController.h"
#import "DBManager.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[DBManager sharedManager] createDb];
    [[DBManager sharedManager] createConnectionTable_A];
    [[DBManager sharedManager] createConnectionTable_B];
    
    [[DBManager sharedManager] sequentialRecordInsertion];
    [[DBManager sharedManager] concurrentRecordsInsertion];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
