//
//  AppDelegate.h
//  DBConcurrencyPOC
//
//  Created by Anshad M K on 22/02/18.
//  Copyright © 2018 Anshad M K. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

